#!/usr/bin/env python

import re

#hash:oft_blake2s endpoints:67108864 size:32 rekey:4096 init:0 print:0 batch:1
#    0.03 real         0.01 user         0.01 sys

input_file = open('bench_results.txt', 'r')
results = input_file.read()

hashes = re.findall('hash:([^\s]+)', results);
endpoints = re.findall('endpoints:(\d+)', results);
sizes = re.findall('size:(\d+)', results);
rekeys = re.findall('rekey:(\d+)', results);
inits = re.findall('init:(\d+)', results);
prints = re.findall('print:(\d+)', results);
batches = re.findall('batch:(\d+)', results);
reals = re.findall('real (\d+\.?\d*)', results);
users = re.findall('user (\d+\.?\d*)', results);
syss = re.findall('sys (\d+\.?\d*)', results);

data = zip(hashes, endpoints, sizes, rekeys, inits, prints, batches, reals, users, syss)


output_file = open('bench_results.tsv', 'w')
for hsh,end,size,rekey,init,prnt,batch,real,usr,sys in data:
    result = '%s\t%s'%(rekey, real)
    output_file.write(result+'\n')
    print result
