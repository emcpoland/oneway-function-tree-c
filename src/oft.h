#ifndef OFT_H
#define OFT_H

#include <stddef.h>
#include <stdint.h>

typedef struct oft_internal *oft;
typedef void (*oft_hash_function)(uint8_t * out, const uint8_t * in, size_t size);

oft create_oft(size_t keysize_bytes, uintmax_t max_endpoints, const oft_hash_function hash);
void init_tree(oft tree);
void destroy(oft tree);

const uint8_t * get_node_key(oft tree, uintmax_t i);
const uint8_t * get_node_blinded_key(oft tree, uintmax_t i);

uintmax_t get_parent_index(uintmax_t i);
uintmax_t get_sibling_index(uintmax_t i);
uintmax_t get_uncle_index(uintmax_t i);
uintmax_t get_left_child_index(uintmax_t i);
uintmax_t get_right_child_index(uintmax_t i);
uintmax_t get_node_index(uintmax_t y, uintmax_t x);
uintmax_t get_leaf_node_index(oft tree, uintmax_t x);

void rekey(oft tree, const uintmax_t x);
void rekey_batch(oft tree, const uintmax_t* nodes_x, const size_t nodes_x_size);

void print_tree(oft tree);
void print_node(oft tree, uintmax_t node_i);
void print_key(const uint8_t* key, size_t size);

#endif /* OFT_H */
