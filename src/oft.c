#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#include "oft.h"
#include "../third_party/hash_set.h"

void init_tree(oft tree);
void xor(uint8_t* out, const uint8_t* a, const uint8_t* b, const size_t size);

void rekey_inner_node(oft tree, const uintmax_t i);
void rekey_leaf_node(oft tree, const uintmax_t x, int urandom_file);
uintmax_t ensure_power_of_2(const uintmax_t a);

struct oft_internal {
	const uintmax_t num_nodes, max_depth, max_endpoints;
	const size_t keysize_bytes;
	const oft_hash_function hash;
	uint8_t nodes[];
};

oft create_oft(size_t keysize_bytes, uintmax_t max_endpoints, const oft_hash_function hash) {
	oft tree;
	const uintmax_t num_nodes = (max_endpoints*2)-1;

	max_endpoints = ensure_power_of_2(max_endpoints);

	// Assign space for the tree and configure metadata
	tree =  malloc(sizeof(*tree)+keysize_bytes*num_nodes*2);
	*(uintmax_t *)&tree->num_nodes = num_nodes;
	*(uintmax_t *)&tree->max_depth = (int)log2(max_endpoints);
	*(uintmax_t *)&tree->max_endpoints = max_endpoints;
	*(size_t *)&tree->keysize_bytes = keysize_bytes;
	*(oft_hash_function *)&tree->hash = hash;

	return tree;
}

void init_leaf_nodes(oft tree) {
	int urandom_file = open("/dev/urandom", O_RDONLY);
		for(uintmax_t i=get_leaf_node_index(tree, 0); i<tree->num_nodes; ++i) {
			rekey_leaf_node(tree, i, urandom_file);
		}
	close(urandom_file);
}

void init_inner_nodes(oft tree) {
	// Calulate inner nodes, by hashing from leaf nodes
	// Gets the first bottom right parent node, and works its way left and up
	 // Its gonna wrap, hence <
	for(uintmax_t node_i = get_leaf_node_index(tree, 0)-1; node_i<tree->max_endpoints; --node_i) {
		rekey_inner_node(tree, node_i);
	}
}

void init_tree(oft tree) {
	init_leaf_nodes(tree);
	init_inner_nodes(tree);
}

void destroy(oft tree) {
	free(tree);
	tree = NULL;
}

const uint8_t * get_node_key(oft tree, uintmax_t i) {
	return &tree->nodes[i*tree->keysize_bytes*2];
}

const uint8_t * get_node_blinded_key(oft tree, uintmax_t i) {
	return &tree->nodes[i*tree->keysize_bytes*2 + tree->keysize_bytes];
}

uintmax_t get_parent_index(uintmax_t i) {
	return (i == 0) ? 0 : (i-1)/2;
}

uintmax_t get_sibling_index(uintmax_t i) {
	return (i == 0) ? 0 : (i%2 == 0) ? --i : ++i;
}

uintmax_t get_uncle_index(uintmax_t i) {
	return (i == 0) ? 0 : get_sibling_index(get_parent_index(i));
}

uintmax_t get_left_child_index(uintmax_t i) {
	return i*2+1;
}

uintmax_t get_right_child_index(uintmax_t i) {
	return get_left_child_index(i)+1;
}

uintmax_t get_node_index(uintmax_t y, uintmax_t x) {
	return pow(2, y)-1+x;
}

uintmax_t get_leaf_node_index(oft tree, uintmax_t i) {
	return tree->num_nodes-tree->max_endpoints+i;
}

////////////////////////////////////////////////////////////////

void rekey_inner_node(oft tree, const uintmax_t i) {
	xor(
		(uint8_t *)get_node_key(tree, i),
		get_node_blinded_key(tree, get_left_child_index(i)),
		get_node_blinded_key(tree, get_right_child_index(i)),
		tree->keysize_bytes
		);

	tree->hash(
		(uint8_t *)get_node_blinded_key(tree, i),
		get_node_key(tree, i),
		tree->keysize_bytes
		);
}

void rekey_leaf_node(oft tree, const uintmax_t i, int urandom_file) {
	read(urandom_file, (uint8_t *)get_node_key(tree, i),  tree->keysize_bytes);

	tree->hash(
		(uint8_t *)get_node_blinded_key(tree, i),
		get_node_key(tree, i),
		tree->keysize_bytes
		);
}

void rekey(oft tree, const uintmax_t x) {
	uintmax_t i = get_leaf_node_index(tree, x);

	int urandom_file = open("/dev/urandom", O_RDONLY);
	rekey_leaf_node(tree, i, urandom_file);
	close(urandom_file);

	while(i != 0) {
		i = get_parent_index(i);
		rekey_inner_node(tree , i);
	}
}

// "Hashset" minimal perfect hash function
// Converts a hashset into a descending ordered set
uintmax_t hash_int(const void *number) {
  return -1-(*(uintmax_t *)number);
}

void rekey_batch(oft tree, const uintmax_t* nodes_x, const size_t nodes_x_size) {
	hash_set_st *set = hash_set_init(hash_int);

	int urandom_file = open("/dev/urandom", O_RDONLY);
	for(uintmax_t node_i, x=0; x<nodes_x_size; ++x) {

		// Rekeys leaf nodes
		node_i = get_leaf_node_index(tree, nodes_x[x]);
		rekey_leaf_node(tree, node_i, urandom_file);

		while(node_i != 0) {
			node_i = get_parent_index(node_i);
			// If set already contains parent, then break loop, move onto next endpoint
			if(hash_set_insert(set, &node_i, sizeof(uintmax_t))==DUPLICATE) break;
		}
	}

	close(urandom_file);

    hash_set_it *it = it_init(set);
    if(it!=NULL) { // Set may be empty if tree only has a single node
	    uintmax_t node_i;
	    do {
	    	node_i = *(uintmax_t *)it_value(it);
			rekey_inner_node(tree , node_i);
		} while(it_next(it)==OK);
	}
	it_free(it);

	hash_set_free(set);
}

////////////////////////////////////////////////////////////////

void xor(uint8_t* out, const uint8_t* a, const uint8_t* b, const size_t size) {
	for(uint8_t i=0; i<size; ++i) out[i] = a[i]^b[i];
}

uintmax_t ensure_power_of_2(const uintmax_t a) {
	const int is_power_of_2 = !(a == 0) && !(a & (a - 1));
	if(is_power_of_2) return a;
	else {
		uintmax_t a_rounded = pow(2, (int)log2(a)+1);
		fprintf(stderr, "\n\nValue not power of 2, increased %lu -> %lu\n\n", a, a_rounded);
		return a_rounded;
	}
}

////////////////////////////////////////////////////////////////

void print_tree(oft tree) {
	printf("\n");
	for(uintmax_t y=0,max_x=1; y<=tree->max_depth; ++y, max_x*=2) {
		for(uintmax_t node_i, x=0; x<max_x; ++x) {
			node_i = get_node_index(y, x);
			print_node(tree, node_i);
			printf("\t");
		}
		printf("\n");
	}
	printf("\n");
}

void print_node(oft tree, uintmax_t node_i) {
	print_key(get_node_key(tree, node_i), tree->keysize_bytes);
	printf(" [ ");
	print_key(get_node_blinded_key(tree, node_i), tree->keysize_bytes);
	printf(" ]");
}

void print_key(const uint8_t* key, size_t size) {
	for(uint8_t i=0; i<size; ++i) printf("%02x",key[i]);
}

////////////////////////////////////////////////////////////////

