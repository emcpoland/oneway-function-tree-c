#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "oft.h"
#include "../third_party/blake2b.h"
#include "../third_party/blake2s.h"
#include "../third_party/sha3.h"
#include "../third_party/sha2.h"

/////////////////////////////////////////

#define INIT_TREE 0
#define PRINT_DEBUG 0
#define BATCH_OPTIMISIATION 1
#define REKEY_CONSEQUTIVE_LEAF_NODES 1
#define HASH_FUNCTION oft_blake2s

/////////////////////////////////////////

#define EXIT_SUCCESS 0
#define EXIT_ERROR -1

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

///////////////////////////////////////// HASH FUNCTIONS

void oft_blake2b(uint8_t * out, const uint8_t * in, const size_t size) {
	blake2b(out, size, NULL, 0, in, size);
}

void oft_blake2s(uint8_t * out, const uint8_t * in, const size_t size) {
	blake2s(out, size, NULL, 0, in, size);
}

void oft_sha2(uint8_t * out, const uint8_t * in, const size_t size) {
	switch(size) {
		case 28:
			sha224(in, size, out);
			break;
		case 32:
			sha256(in, size, out);
			break;
		case 48:
			sha384(in, size, out);
			break;
		case 64:
			sha512(in, size, out);
			break;
		default:
			exit(EXIT_ERROR);
	}
}

void oft_sha3(uint8_t * out, const uint8_t * in, const size_t size) {
	sha3(in, size, out, size);
}

///////////////////////////////////////// MAIN

int main(int argc, char **argv) {

	///////////////////////////////////////////////////////////////// READ INPUT PARAMETERS /////////////////////////////////////////////////////////////////

	const uintmax_t input_max_endpoints = (argc>1) ?  atol(argv[1]) : 8;
	const uintmax_t input_rekey_amount = (argc>2) ?  atol(argv[2]) : 1;
	const size_t input_key_size = (argc>3) ?  atol(argv[3]) : 32;

	printf("\n\thash:%s endpoints:%lu size:%lu rekey:%lu init:%d print:%d batch:%d\n",
		TOSTRING(HASH_FUNCTION), input_max_endpoints, input_key_size, input_rekey_amount, INIT_TREE, PRINT_DEBUG, BATCH_OPTIMISIATION);

	//////////////////////////////////////////////////////////////////// GENERATE TREE //////////////////////////////////////////////////////////////////////

	oft tree = create_oft(input_key_size, input_max_endpoints, HASH_FUNCTION);

	#if INIT_TREE
		init_tree(tree);
	#endif

	#if PRINT_DEBUG
		print_tree(tree);
	#endif

	///////////////////////////////////////////////////////////////////// REKEY TREE ////////////////////////////////////////////////////////////////////////

	#if BATCH_OPTIMISIATION
		// input_rekey_amount > size_t therefore use pointer over array
		uintmax_t * rekey_nodes = malloc(sizeof(uintmax_t)*input_rekey_amount);
		for(uintmax_t i=0; i<input_rekey_amount; ++i) {
			#if REKEY_CONSEQUTIVE_LEAF_NODES
				rekey_nodes[i] = i;
			#else
				rekey_nodes[i] = i*input_max_endpoints/input_rekey_amount;
			#endif
		}
		if(input_rekey_amount > 0) rekey_batch(tree, rekey_nodes, input_rekey_amount);
		free(rekey_nodes);
	#else
		for(uintmax_t i=0; i<input_rekey_amount; ++i) {
			#if REKEY_CONSEQUTIVE_LEAF_NODES
				rekey(tree, i);
			#else
				rekey(tree, i*input_max_endpoints/input_rekey_amount);
			#endif
		}
	#endif

	#if PRINT_DEBUG
		print_tree(tree);
	#endif

	destroy(tree);
	return EXIT_SUCCESS;
}
