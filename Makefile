.PHONY: all compile run debug memcheck bench sky tests

CC = gcc #gcc-6
LIBS = -lm
CFLAGS = -std=c11 -g -Ofast -Wall -Wextra -Werror #-fopenmp
SRCS = ./src/*.c ./third_party/*.c
TARGET = ./build/main

all: run

compile:
	@mkdir -p build
	$(CC) -o $(TARGET) $(SRCS) $(LIBS) $(CFLAGS)

run: compile
	$(TARGET)

debug: compile
	gdb $(TARGET)

memcheck:
	valgrind --leak-check=full $(TARGET)

sky: compile
	@time -p $(TARGET) 16777216 1176000 32

tests:
	cd tests; make all;

clean:
	rm -rf build
	cd tests; make clean;

system.info:
	lsb_release -a
	cat /proc/cpuinfo
	grep MemTotal /proc/meminfo

bench: bench.endpoints bench.rekey bench.rekey.ten #bench.keysize #bench.rekey.many

bench.endpoints: compile
	@echo
	@echo "ENDPOINTS"
	@time -p $(TARGET) 1 0
	@time -p $(TARGET) 2 0
	@time -p $(TARGET) 4 0
	@time -p $(TARGET) 8 0
	@time -p $(TARGET) 16 0
	@time -p $(TARGET) 32 0
	@time -p $(TARGET) 64 0
	@time -p $(TARGET) 128 0
	@time -p $(TARGET) 256 0
	@time -p $(TARGET) 512 0
	@time -p $(TARGET) 1024 0
	@time -p $(TARGET) 2048 0
	@time -p $(TARGET) 4096 0
	@time -p $(TARGET) 8192 0
	@time -p $(TARGET) 16384 0
	@time -p $(TARGET) 32768 0
	@time -p $(TARGET) 65536 0
	@time -p $(TARGET) 131072 0
	@time -p $(TARGET) 262144 0
	@time -p $(TARGET) 524288 0
	@time -p $(TARGET) 1048576 0
	@time -p $(TARGET) 2097152 0
	@time -p $(TARGET) 4194304 0
	#@time -p $(TARGET) 8388608 0
	#@time -p $(TARGET) 16777216 0
	#@time -p $(TARGET) 33554432 0
	#@time -p $(TARGET) 67108864 0

bench.rekey: compile
	@echo
	@echo "REKEY"
	@time -p $(TARGET) 4194304 0
	@time -p $(TARGET) 4194304 1
	@time -p $(TARGET) 4194304 2
	@time -p $(TARGET) 4194304 4
	@time -p $(TARGET) 4194304 8
	@time -p $(TARGET) 4194304 16
	@time -p $(TARGET) 4194304 32
	@time -p $(TARGET) 4194304 64
	@time -p $(TARGET) 4194304 128
	@time -p $(TARGET) 4194304 256
	@time -p $(TARGET) 4194304 512
	@time -p $(TARGET) 4194304 1024
	@time -p $(TARGET) 4194304 2048
	@time -p $(TARGET) 4194304 4096
	@time -p $(TARGET) 4194304 8192
	@time -p $(TARGET) 4194304 16384
	@time -p $(TARGET) 4194304 32768
	@time -p $(TARGET) 4194304 65536
	@time -p $(TARGET) 4194304 131072
	@time -p $(TARGET) 4194304 262144
	@time -p $(TARGET) 4194304 524288
	@time -p $(TARGET) 4194304 1048576
	@time -p $(TARGET) 4194304 2097152
	@time -p $(TARGET) 4194304 4194304

bench.rekey.fit: compile
	@echo
	@echo "REKEY"
	@time -p $(TARGET) 1 1
	@time -p $(TARGET) 2 2
	@time -p $(TARGET) 4 4
	@time -p $(TARGET) 8 8
	@time -p $(TARGET) 16 16
	@time -p $(TARGET) 32 32
	@time -p $(TARGET) 64 64
	@time -p $(TARGET) 128 128
	@time -p $(TARGET) 256 256
	@time -p $(TARGET) 512 512
	@time -p $(TARGET) 1024 1024
	@time -p $(TARGET) 2048 2048
	@time -p $(TARGET) 4096 4096
	@time -p $(TARGET) 8192 8192
	@time -p $(TARGET) 16384 16384
	@time -p $(TARGET) 32768 32768
	@time -p $(TARGET) 65536 65536
	@time -p $(TARGET) 131072 131072
	@time -p $(TARGET) 262144 262144
	@time -p $(TARGET) 524288 524288
	@time -p $(TARGET) 1048576 1048576
	@time -p $(TARGET) 2097152 2097152
	@time -p $(TARGET) 4194304 4194304

bench.rekey.many: compile
	@echo
	@echo "REKEY MANY"
	@time -p $(TARGET) 67108864 0
	@time -p $(TARGET) 67108864 1
	@time -p $(TARGET) 67108864 2
	@time -p $(TARGET) 67108864 4
	@time -p $(TARGET) 67108864 8
	@time -p $(TARGET) 67108864 16
	@time -p $(TARGET) 67108864 32
	@time -p $(TARGET) 67108864 64
	@time -p $(TARGET) 67108864 128
	@time -p $(TARGET) 67108864 256
	@time -p $(TARGET) 67108864 512
	@time -p $(TARGET) 67108864 1024
	@time -p $(TARGET) 67108864 2048
	@time -p $(TARGET) 67108864 4096
	@time -p $(TARGET) 67108864 8192
	@time -p $(TARGET) 67108864 16384
	@time -p $(TARGET) 67108864 32768
	@time -p $(TARGET) 67108864 65536
	@time -p $(TARGET) 67108864 131072
	@time -p $(TARGET) 67108864 262144
	@time -p $(TARGET) 67108864 524288
	@time -p $(TARGET) 67108864 1048576
	@time -p $(TARGET) 67108864 2097152
	@time -p $(TARGET) 67108864 4194304
	@time -p $(TARGET) 67108864 8388608
	@time -p $(TARGET) 67108864 16777216
	@time -p $(TARGET) 67108864 33554432
	@time -p $(TARGET) 67108864 67108864

bench.rekey.ten: compile
	@echo
	@echo "REKEY TEN"
	@time -p $(TARGET) 1048576 100000
	@time -p $(TARGET) 1048576 200000
	@time -p $(TARGET) 1048576 300000
	@time -p $(TARGET) 1048576 400000
	@time -p $(TARGET) 1048576 500000
	@time -p $(TARGET) 1048576 600000
	@time -p $(TARGET) 1048576 700000
	@time -p $(TARGET) 1048576 800000
	@time -p $(TARGET) 1048576 900000
	@time -p $(TARGET) 1048576 1000000

bench.keysize: compile
	@echo
	@echo "REKEY TEN"
	#@time -p $(TARGET) 4194304 4194304 20
	#@time -p $(TARGET) 4194304 4194304 24
	@time -p $(TARGET) 4194304 4194304 28 # sha2/3
	@time -p $(TARGET) 4194304 4194304 32 # sha2/3
	#@time -p $(TARGET) 4194304 4194304 36
	#@time -p $(TARGET) 4194304 4194304 40
	#@time -p $(TARGET) 4194304 4194304 44
	@time -p $(TARGET) 4194304 4194304 48 # sha2/3
	#@time -p $(TARGET) 4194304 4194304 52
	#@time -p $(TARGET) 4194304 4194304 56
	#@time -p $(TARGET) 4194304 4194304 60
	@time -p $(TARGET) 4194304 4194304 64 # sha2/3

login:
	ssh emcpoland01@delllogin.qub.ac.uk

sync:
	#--delete
	rsync -rav -e ssh --delete --exclude='.*' ./ emcpoland01@delllogin.qub.ac.uk:/home/emcpoland01/sync/oft/

qsub:
	qsub ./scripts/bench.qsub

qsub.results:
	@mkdir -p results
	sftp emcpoland01@delllogin.qub.ac.uk:/home/emcpoland01/sync/oft/bench.out ./results/
