#include <cgreen/cgreen.h>

TestSuite *tree_tests();
TestSuite *rekey_tests();

int main(int argc, char **argv) {
	TestSuite *suite = create_test_suite();
	add_suite(suite, tree_tests());
	add_suite(suite, rekey_tests());

	if (argc > 1) return run_single_test(suite, argv[1], create_text_reporter());
	else return run_test_suite(suite, create_text_reporter());
}
