#include <stdio.h>
#include <cgreen/cgreen.h>

#include "../src/oft.h"

/*
0                                          A123[Z123]
1                    B123[Y123]                                    C123[X123]
2         D123[W123]            E123[V123]             F123[U123]             G123[T123]
3  H123[S123] I123[R123]  J123[Q123] K123[P123]  L123[O123] M123[N123]  N123[M123] O123[L123]
X      0          1           2          3           4          5           6          7
*/

static oft tree;

void dummy_hash(__attribute__((unused)) uint8_t * out, __attribute__((unused))  const uint8_t * in, __attribute__((unused))  const size_t size) {}

static void init_test_tree(oft tree) {
	// Hijacks the tree to insert custom test data
	uint8_t * nodes = (uint8_t *)get_node_key(tree, 0);
	uint8_t nodes_pre[] = "A123Z123B123Y123C123X123D123W123E123V123F123U123G123T123H123S123I123R123J123Q123K123P123L123O123M123N123N123M123O123L123";
	memcpy(nodes, nodes_pre, strlen((const char *)nodes_pre));
}

static void setup() {
	tree = create_oft(4, 8, dummy_hash);
	init_test_tree(tree);
}

Ensure(test_get_node) {
	uint8_t key_results[15], blinded_results[15];

	for(int i=0; i<15; ++i) key_results[i] = *get_node_key(tree, i);
	uint8_t key_expected[] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O'};

	for(int i=0; i<15; ++i) blinded_results[i] = *get_node_blinded_key(tree, i);
	uint8_t blinded_expected[] = {'Z','Y','X','W','V','U','T','S','R','Q','P','O','N','M','L'};

	assert_that(sizeof(key_results), is_equal_to(sizeof(key_expected)));
	assert_that(key_results, is_equal_to_contents_of(key_expected, sizeof(key_expected)));

	assert_that(sizeof(blinded_results), is_equal_to(sizeof(blinded_expected)));
	assert_that(blinded_results, is_equal_to_contents_of(blinded_expected, sizeof(blinded_expected)));
}

Ensure(test_get_parent_index) {
	uintmax_t results[15];
	for(int i=0; i<15; ++i) results[i] = get_parent_index(i);

	uintmax_t expected[] = {0, 0,0,1,1,2,2,3,3,4,4,5,5,6,6};

	assert_that(sizeof(results), is_equal_to(sizeof(expected)));
	assert_that(results, is_equal_to_contents_of(expected, sizeof(expected)));
}

Ensure(test_get_sibling_index) {
	uintmax_t results[15];
	for(int i=0; i<15; ++i) results[i] = get_sibling_index(i);

	uintmax_t expected[] = {0, 2,1,4,3,6,5,8,7,10,9,12,11,14,13};

	assert_that(sizeof(results), is_equal_to(sizeof(expected)));
	assert_that(results, is_equal_to_contents_of(expected, sizeof(expected)));
}

Ensure(test_get_uncle_index) {
	uintmax_t results[15];
	for(int i=0; i<15; ++i) results[i] = get_uncle_index(i);

	uintmax_t expected[] = {0, 0,0,2,2,1,1,4,4,3,3,6,6,5,5};

	assert_that(sizeof(results), is_equal_to(sizeof(expected)));
	assert_that(results, is_equal_to_contents_of(expected, sizeof(expected)));
}

Ensure(test_get_left_child_index) {
	uintmax_t results[7];
	for(int i=0; i<7; ++i) results[i] = get_left_child_index(i);

	uintmax_t expected[] = {1,3,5,7,9,11,13};

	assert_that(sizeof(results), is_equal_to(sizeof(expected)));
	assert_that(results, is_equal_to_contents_of(expected, sizeof(expected)));
}

Ensure(test_get_right_child_index) {
	uintmax_t results[7];
	for(int i=0; i<7; ++i) results[i] = get_right_child_index(i);

	uintmax_t expected[] = {2,4,6,8,10,12,14};

	assert_that(sizeof(results), is_equal_to(sizeof(expected)));
	assert_that(results, is_equal_to_contents_of(expected, sizeof(expected)));
}

Ensure(test_get_node_index) {
	uintmax_t results[] = {
		get_node_index(0,0),
		get_node_index(1,0), get_node_index(1,1),
		get_node_index(2,0), get_node_index(2,1), get_node_index(2,2), get_node_index(2,3),
		get_node_index(3,0), get_node_index(3,1), get_node_index(3,2), get_node_index(3,3), get_node_index(3,4), get_node_index(3,5), get_node_index(3,6), get_node_index(3,7)
	};

	uintmax_t expected[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14};

	assert_that(sizeof(results), is_equal_to(sizeof(expected)));
	assert_that(results, is_equal_to_contents_of(expected, sizeof(expected)));
}

Ensure(test_get_leaf_node_index) {
	uintmax_t results[8];
	for(int i=0; i<8; ++i) results[i] = get_leaf_node_index(tree, i);

	uintmax_t expected[] = {7,8,9,10,11,12,13,14};

	assert_that(sizeof(results), is_equal_to(sizeof(expected)));
	assert_that(results, is_equal_to_contents_of(expected, sizeof(expected)));
}

static void teardown() {
	destroy(tree);
}

TestSuite *tree_tests() {
	TestSuite *suite = create_test_suite();
	set_setup(suite, setup);
	set_teardown(suite, teardown);
	add_test(suite, test_get_node);
	add_test(suite, test_get_parent_index);
	add_test(suite, test_get_sibling_index);
	add_test(suite, test_get_uncle_index);
	add_test(suite, test_get_left_child_index);
	add_test(suite, test_get_right_child_index);
	add_test(suite, test_get_node_index);
	add_test(suite, test_get_leaf_node_index);
	return suite;
}
