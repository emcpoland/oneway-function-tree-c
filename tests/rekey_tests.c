#include <stdio.h>
#include <cgreen/cgreen.h>
#include "../src/oft.h"
#include "../third_party/blake2b.h"

#define KEY_SIZE 32
#define MAX_ENDPOINTS 16
#define REKEY_AMOUNT 2

static oft tree;

static void hash(uint8_t * out, const uint8_t * in, const size_t size) {
	blake2b(out, size, NULL, 0, in, size);
}

void xor(uint8_t* out, const uint8_t* a, const uint8_t* b, const size_t size);
void ensure_valid_tree();

static void setup() {
	tree = create_oft(KEY_SIZE, MAX_ENDPOINTS, hash);
	init_tree(tree);
}

Ensure(test_init) {
	ensure_valid_tree();
}

Ensure(test_single_rekey) {
	rekey(tree, REKEY_AMOUNT);
	ensure_valid_tree();
}

Ensure(test_batch_rekey_best_case) {
	const uintmax_t input_rekey_amount = REKEY_AMOUNT;

	// (input_rekey_amount) uintmax_t > size_t therefore use pointer over array
	uintmax_t * rekey_nodes = malloc(sizeof(uintmax_t)*input_rekey_amount);
	for(uintmax_t i=0; i<input_rekey_amount; ++i) rekey_nodes[i] = i;
	if(input_rekey_amount > 0) rekey_batch(tree, rekey_nodes, input_rekey_amount);
	free(rekey_nodes);

	ensure_valid_tree();
}

Ensure(test_batch_rekey_worst_case) {
	const uintmax_t input_rekey_amount = REKEY_AMOUNT;

	// (input_rekey_amount) uintmax_t > size_t therefore use pointer over array
	uintmax_t * rekey_nodes = malloc(sizeof(uintmax_t)*input_rekey_amount);
	for(uintmax_t i=0; i<input_rekey_amount; ++i) rekey_nodes[i] = i*MAX_ENDPOINTS/input_rekey_amount;
	if(input_rekey_amount > 0) rekey_batch(tree, rekey_nodes, input_rekey_amount);
	free(rekey_nodes);

	ensure_valid_tree();
}

static void teardown() {
	destroy(tree);
}

TestSuite *rekey_tests() {
	TestSuite *suite = create_test_suite();
	set_setup(suite, setup);
	set_teardown(suite, teardown);
	add_test(suite, test_init);
	add_test(suite, test_single_rekey);
	add_test(suite, test_batch_rekey_best_case);
	add_test(suite, test_batch_rekey_worst_case);
	return suite;
}

/////////////////////////////////////////////// VALIDATORS

void ensure_valid_group_key() {
	uint8_t expected_key[KEY_SIZE];

	for(uintmax_t e = 0; e < MAX_ENDPOINTS; e++) {
		uintmax_t node_i = get_leaf_node_index(tree, e);
		memcpy(expected_key, get_node_blinded_key(tree, get_sibling_index(node_i)), KEY_SIZE);
		do {
			xor(expected_key, expected_key, get_node_blinded_key(tree, node_i), KEY_SIZE);
			hash(expected_key, expected_key, KEY_SIZE);
			node_i = get_uncle_index(node_i);
		} while (node_i > 0);

		assert_that(get_node_blinded_key(tree, 0), is_equal_to_contents_of(expected_key, KEY_SIZE));
	}
}

void ensure_valid_blinded_keys() {
	const uintmax_t max_nodes = 2*MAX_ENDPOINTS-1;
	uint8_t expected_key[KEY_SIZE];

	for(uintmax_t i = 0; i < max_nodes-1; ++i) {
		hash(expected_key, get_node_key(tree, i), KEY_SIZE);
		assert_that(get_node_blinded_key(tree, i), is_equal_to_contents_of(expected_key, KEY_SIZE));
	}
}

void ensure_unique_keys() {
	const uintmax_t max_nodes = 2*MAX_ENDPOINTS-1;

	uint8_t *this, *that, *this_blinded, *that_blinded;
	for(uintmax_t i = 0; i < max_nodes-1; ++i) {
		this = (uint8_t * )get_node_key(tree, i);
		this_blinded = (uint8_t * )get_node_blinded_key(tree, i);

		for(uintmax_t j = i+1; j < max_nodes; ++j) {
			that = (uint8_t * )get_node_key(tree, j);
			that_blinded = (uint8_t * )get_node_blinded_key(tree, j);

			assert_that(this, is_not_equal_to_contents_of(that, KEY_SIZE));
			assert_that(this, is_not_equal_to_contents_of(that_blinded, KEY_SIZE));
			assert_that(this_blinded, is_not_equal_to_contents_of(that_blinded, KEY_SIZE));
		}
	}
}

void ensure_valid_tree() {
	ensure_valid_group_key();
	ensure_unique_keys();
	ensure_valid_blinded_keys();
}
